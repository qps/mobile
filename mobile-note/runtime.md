# 移动端环境搭建
1. 利用 chrome 浏览器  deviceToolbar 来调试移动端页面的尺寸    
2. 利用本地服务器创建端口,进行真机调试.
	本地创建wifi-手机进行链接-获取电脑是ip-在电脑打开移动项目-把移动项目的ip更改成我们电脑的ip-把地址复制到手机浏览器上打开
3. 利用hbuilder等工具进行虚拟机调试 
4. 直接把手机连上电脑。最常用的，让程序运行在手机上。步骤：电脑安装手机驱动（识别手机）-	手机设置：版本号（进入开发者模式）-然后退回来-开发人原选项开启，usb开启-这样手机就能运行电脑的程序-数据线连接-重启hbuilder-打开响应的移动项目-然后点击在移动端运行
    
